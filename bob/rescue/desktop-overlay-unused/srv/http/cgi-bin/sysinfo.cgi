#!/bin/bash
echo "Content-type: text/html"
echo ""
echo "<html><head><title>sysinfo"
echo "</title></head><body>"

echo "<h1>General system information for host $(hostname -s)</h1>"
echo ""

echo "<h2>Memory Info</h2>"
echo "<pre> $(free -m) </pre>"

echo "<h2>Disk Info:</h2>"
echo "<pre> $(df -h) </pre>"

echo "<center>Information generated on $(date)</center>"
echo "</body></html>"